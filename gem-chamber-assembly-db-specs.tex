\documentclass[10pt,a4paper]{refart}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[english]{isodate}
\usepackage[parfill]{parskip}

\usepackage{float}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{color,xcolor}
\usepackage{textcomp,tipa}

\usepackage{amssymb}
\usepackage[normalem]{ulem}
\usepackage{hyperref}
\usepackage{feynmp}
\usepackage{etoolbox}
\usepackage{ifpdf}
\ifpdf
\DeclareGraphicsRule{*}{mps}{*}{}
\fi
\usepackage{multirow}
\usepackage{subfigure}
\usepackage{pdflscape}
%% \usepackage{subfig}
\usepackage{textpos}
\usepackage{pdfpages}
\usepackage{tikz}

\usepackage{pgfplots}
\usetikzlibrary{positioning}
% ,positioning-plus,node-families}
% \usetikzlibrary{geometric,fit}
\usetikzlibrary{fit}
\usetikzlibrary{backgrounds}
\usetikzlibrary{calc}
\usetikzlibrary{shapes,shadows,arrows}
\usetikzlibrary{mindmap}
\usetikzlibrary{decorations.text}
\pgfplotsset{compat=1.7}

\title{GEM Chamber Assembly DB Functional Specifications}
\author{GEM Commissioining, Operations, and Online Software Group}
\date{\today}

\begin{document}

\maketitle

\section*{Outline}
\label{sec:outline}
This document aims to provide a concise set of functional requirements for the chamber assembly needs of the GEM project.
There is some possible overlap between this specificaitonand the QC7 specification, as the replacement of components MAY be done by the chamber assembly team or the QC7 team following a QC7 test result.

\input{gem-spec-common-outline}

\input{gem-spec-common-terminology}

\newpage

\section{Details}
\label{sec:details}

\subsection{Tables}
\label{subsec:details:tables}
All component information will live in the \texttt{PARTS} table in the \texttt{CMS\_GEM\_CORE\_CONSTRUCT} schema.
The chamber and superchamber specific information is aggregated by use of views in the \texttt{CMS\_GEM\_MUON\_VIEW} schema.
There are common fields for all entries in this table and schema.
Detailed in the subsections below are the per-component specific fields that are REQUIRED.

\subsubsection{Component tables}
The individual (tracked) components are listed in Table~\ref{tab:commonpart}.
\begin{table}[!h]
  \label{tab:commonpart}
  \caption{Common fields for the \texttt{PARTS} table}
  \begin{tabular}[!h]{p{0.2\textwidth}|p{0.8\textwidth}}
    Component & Description\\
    \hline
    \hline
    OptoHybrid & \texttt{PARTS} table \\
    GEB        & \texttt{PARTS} table \\
    VFAT       & \texttt{PARTS} table \\
    GBTx       & \texttt{PARTS} table \\
    FEAST      & MAY NOT be tracked in the DB \\
    VTRx       & MAY NOT be tracked in the DB \\
    VTTx       & MAY NOT be tracked in the DB \\
    \hline
  \end{tabular}
\end{table}


\subsubsection{\texttt{Chamber}}
GEM chambers are composed of three foils, a readout board, an external frame.
On the readout board are mounted the readout electronics.
Depending on the station of the chamber, different combinations of components are mounted\footnotetext[1]{For GE2/1, the ``chambers'' are composed of 4 separate modules, and each module is associated with the components listed in Table~\ref{tab:chambergens}}:

\begin{table}[!h]
  \label{tab:chambergens}
  \caption{Electronics component differences for different GEM detector stations}
  \begin{tabular}[!h]{p{0.3\textwidth}|p{0.2\textwidth}|p{0.2\textwidth}|p{0.2\textwidth}}
    & GE1/1 & GE2/1\footnotemark[1] & ME0 \\
    \hline
    Component &  &  &       \\
    \hline
    GEB & 2 & 1* & 1        \\
    OptoHybrid & 1 & 1* & 0 \\
    VFAT & 24 & 12* & 24    \\
    \hline
  \end{tabular}
\end{table}

The REQUIRED information is listed in Table~\ref{tab:chamber}.
Several optimizations could be foreseen, namely, in the Chamber table/view, the ``Temperature Sensor ID'' and ``Temperature Chain ID'' MAY be redundant, the ``Temperature Chain'' SHOULD be defined in the ``Temperature Sensor'' table, as a FK linked via the ID.

\begin{table}[!h]
  \label{tab:chamber}
  \caption{Information REQUIRED for the Chamber table/view}
  \begin{tabular}[!h]{p{0.35\textwidth}|p{0.65\textwidth}}
    Entry & Description \\
    \hline
    \hline
    Chamber ID            & PK, S/N of chamber, .e.g., \texttt{GE1/1-X-L-CERN-0028} \\
    Frame ID              & FK link to PK of the Frame table \\
    Drift PCB ID          & FK link to PK of the Drift PCB table \\
    GEM1 foil ID          & FK link to PK of the GEM foil table \\
    GEM2 foil ID          & FK link to PK of the GEM foil table \\
    GEM3 foil ID          & FK link to PK of the GEM foil table \\
    ROB ID                & FK link to PK of the ROB table \\
    GEB ID(s)             & FK link to PK of the GEB table \\
    VFAT(position) IDs    & FKs link to PK of the VFAT table \\
    OptoHybrid ID         & FK link to PK of the OptoHybrid table \\
    Temperature sensor ID & FK link to PK in Temperature sensor table (format is $<C-N>$)  \\
    Temperature chain ID  & \textcolor{red}{red}, \textcolor{yellow}{yellow}, \textcolor{blue}{blue}, \textcolor{green}{green} (derived from the ID), only present on Short chambers, GE1/1\\
    RADMON sensor ID      & FK link to RADMON sensor table, MAY be NULL , only on certain Long chambers, GE1/1\\
    Cooling circuit ID    & FK link to PK of the cooling circuit table \\
    e-log link            & text field for an e-log short URL \\
    Comment               & text field for an arbitrary length character comment\\
    \hline
  \end{tabular}
\end{table}

\subsubsection{\texttt{SuperChamber}}
SuperChambers are composed of two chambers mounted together.
Prior to assembly and QC7, the pair of chambers that will compose a given SC is known as a result of the previous QC steps.
The REQUIRED information is listed in Table~\ref{tab:superchamber}
\begin{table}[!h]
  \label{tab:superchamber}
  \caption{Information REQUIRED for the SC table}
  \begin{tabular}[!h]{p{0.35\textwidth}|p{0.65\textwidth}}
    Entry & Description \\
    \hline
    \hline
    SC ID                & PK, incrementing number for this chamber \\
    Interior Chamber     & FK link to PK of the chamber table \\
    Exterior Chamber     & FK link to PK of the chamber table \\
    Temperature chain ID & \textcolor{red}{red}, \textcolor{yellow}{yellow}, \textcolor{blue}{blue}, \textcolor{green}{green} \\
    e-log link           & text field for an e-log short URL \\
    Comment              & text field for an arbitrary length character comment\\
    \hline
  \end{tabular}
\end{table}

\subsubsection{\texttt{Assembly Report}}
During the assembly proceess variuos tests of the components are performed.
An ``assembly report'' table SHOULD be provided, and linked to the (super)chambers.
The REQUIRED information is listed in Table~\ref{tab:assemblyreport}.
\begin{table}[!h]
  \label{tab:assemblyreport}
  \caption{Information REQUIRED for the assembly report  table}
  \begin{tabular}[!h]{p{0.35\textwidth}|p{0.65\textwidth}}
    Entry & Description \\
    \hline
    \hline
    Chamber ID     & PK, incrementing number for this chamber \\
    Assembly team  & Names (IDs) of the people who assembled the chamber \\
    OH1 Voltage    & measured voltage at the OH1 testpoint \\
    OH2 Voltage    & measured voltage at the OH2 testpoint \\
    OH3 Voltage    & measured voltage at the OH3 testpoint \\
    OH4 Voltage    & measured voltage at the OH4 testpoint \\
    OH5 Voltage    & measured voltage at the OH5 testpoint \\
    OH6 Voltage    & measured voltage at the OH6 testpoint \\
    GND1 Voltage   & measured voltage at the GND1 testpoint \\
    GND2 Voltage   & measured voltage at the GND2 testpoint \\
    GND3 Voltage   & measured voltage at the GND3 testpoint \\
    GND4 Voltage   & measured voltage at the GND4 testpoint \\
    GND5 Voltage   & measured voltage at the GND5 testpoint \\
    GND6 Voltage   & measured voltage at the GND6 testpoint \\
    e-log link     & text field for an e-log short URL \\
    Comment        & text field for an arbitrary length character comment\\
    \hline
  \end{tabular}
\end{table}


\newpage
\section{Requirements}
\label{sec:requirements}

\subsection{Loaders}
\label{subsec:requirements:load}
Two modes of loading data into the database are expected:
\begin{itemize}
\item Manually uploading through a web-based GUI, process described in detail in Section~\ref{subsec:requirements:load:webui}
\item Batch uploading through a CLI using XML templates specific to the table and data being loaded
\end{itemize}

\subsubsection{Web UI}
\label{subsec:requirements:load:webui}
The web based UI currently exists in an incomplete form, and different functionality between development and production.
The primary interactions with the web UI is foreseen to be a crucial part of the normal chamber assembly workflow.
\begin{enumerate}
\item Initial loading of primary data associated with the detector itself
\item Secondary loading/updating of data, i.e., when electronics components are attached
\item Attachment of assembly report
\item Joining of chambers into superchambers.
\end{enumerate}
Some of these functionalities MAY be foreseen to be merged into an ``(Super)Chamber Assembly Report'' tool.

Depending on the station of the chamber being modified, different options and number of attachable components (cf.~\ref{tab:chambergens}) SHOULD be presented to the user.
For the VFATs, the attachment SHOULD also be done in such a way that the mapping can be created/extracted from an appropriate table.

The chamber assembly workflow is as follows:
\begin{enumerate}
\item Select pair of chambers that will form a SC
\item Create chamber and link all detector components prior to first QC step where a chamber (without electronics) is used
\item Prior to the chamber entering QC7, edit chamber object and link all attached electronics components
\item During QC7, the QC7 team MAY perform the replacement of components, and the QC7 interface and functional specification document SHOULD be consulted for the workflow.
  If the responsibility of replacing components falls to the Assembly Team, then this step would be done as per the tools described in the Chamber Assembly (this) document.
\item After successfully passing QC7, link two chambers to SC object
\end{enumerate}

\textbf{Assembling chamber with electronics}
\begin{enumerate}
\item Mount the GEBs on the ROB
\item Mount the OptoHybrid on the GEBs
\item Mount the VFATs on the GEBs and ROB
\item Mount the FEASTs
  \begin{itemize}
  \item Measure the power provided at each test point on the OptoHybrid
    \begin{itemize}
    \item 6 voltage test points (\texttt{OHX})
    \item 6 ground test points (\texttt{GNDX})
    \end{itemize}
  \end{itemize}
\item Select the Chamber from the dropdown list by it's S/N (all subsequent actions MUST link the specified components with the selected chamber in the DB)
\item Select the mounted GEB(s) from the dropdown list by their S/N (depending on th type of chamber, different numbers/types of GEB(s) SHOULD be displayed)
\item Select the mounted OptoHybrid from the dropdown list by its S/N 
\item For each VFAT position, select the mounted VFAT from the dropdown list by their S/N (depending on th type of chamber, different numbers of VFATs SHOULD be displayed)
\item Enter the voltages from the test in their respective fields
\item Enter an elog link
\item Enter any comments, separated per chamber
\item Submit changes
\end{enumerate}

\begin{figure}[!h]
  \hspace*{-30ex}\includegraphics[width=1.5\textwidth,keepaspectratio]{graphics/Chamber_Assembly_Interface.pdf}
\label{fig:qc7ui}
\caption{Sketch of the Chamber Assembly UI}
\end{figure}

\subsubsection{\texttt{dbldr}}
\label{subsec:requirements:load:dbldr}
The \texttt{gemdbldr} is a dropbox style file watcher process, running in a look-area to watch for new files.
The files MUST be zipped, and the XML MUST be valid, corresponding to the table where the dataset is destined.
It is expected that the DB group provides the template for this upload, with the input from the assembly team..

\newpage

\subsection{Extraction}
\label{subsec:requirements:extract}
Extraction is most important for configuration and visualization.

\newpage

\subsection{Validation}
\label{subsec:requirements:validate}
Tools to ensure the validity of the data in the database, as well as the consistency of the data to be loaded are expected.
\begin{itemize}
\item It MUST NOT be possible to attach the same component to multiple chambers, it MUST require an explicit ``detatch'' action to break the connection from one of the chambers.
\item 
\end{itemize}

\newpage

\subsection{Visualization}
\label{subsec:requirements:visuzlize}
The visualization will be done with the CMS online monitoring system (OMS).

\subsubsection{\textbf{Per-week throughput}}

\subsubsection{\textbf{Integrated failed components}}

\subsubsection{\textbf{table of latest entry for each chamber}}

\subsubsection{\textbf{table of all entries for a selected chamber}}

\subsubsection{\textbf{<desired visualization>}}


\newpage

\section{Use cases}
\label{sec:usecases}

\newpage

\section{State of the art}
\label{sec:status}
Currently, implementations of the Web UI exist for both \href{https://gemdb.web.cern.ch/gemdb/}{development} and \href{https://gemdb-p5.web.cern.ch/gemdb-p5/}{production} versions of the GEM DB.
Functionalities that are implemented are:
\begin{itemize}
\item Assembling (and editing) of chambers
\item Assembling (and editing) of superchambers
\end{itemize}

Missing functionalities that need implementation:
\begin{itemize}
\item Single unified interface for the chamber assembly and assemnly report
\end{itemize}

\end{document}
