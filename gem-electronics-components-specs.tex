\documentclass[10pt,a4paper]{refart}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[english]{isodate}
\usepackage[parfill]{parskip}

\usepackage{float}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{color}
\usepackage{xcolor}
\usepackage{textcomp,tipa}

\usepackage{amssymb}
\usepackage[normalem]{ulem}
\usepackage{hyperref}
\usepackage{feynmp}
\usepackage{etoolbox}
\usepackage{ifpdf}
\ifpdf
\DeclareGraphicsRule{*}{mps}{*}{}
\fi
\usepackage{multirow}
\usepackage{subfigure}
\usepackage{pdflscape}
%% \usepackage{subfig}
\usepackage{textpos}
\usepackage{pdfpages}
\usepackage{tikz}

\usepackage{pgfplots}
\usetikzlibrary{positioning}
% ,positioning-plus,node-families}
% \usetikzlibrary{geometric,fit}
\usetikzlibrary{fit}
\usetikzlibrary{backgrounds}
\usetikzlibrary{calc}
\usetikzlibrary{shapes,shadows,arrows}
\usetikzlibrary{mindmap}
\usetikzlibrary{decorations.text}
\pgfplotsset{compat=1.7}

\title{GEM Electronics Components DB Functional Specifications}
\author{GEM Commissioining, Operations, and Online Software Group}
\date{\today}

\begin{document}

\maketitle

\section{Outline}
\label{sec:outline}
This document aims to provide a concise set of functional DB requirements for the DAQ electronics needs of the GEM project.

\input{gem-spec-common-outline}

\input{gem-spec-common-terminology}

\newpage

\section{Details}
\label{sec:details}

\subsection{Tables}
\label{subsec:details:tables}
The electronics component tables will live in the \texttt{<PART>} table in the GEM COND schema.
There are common fields for all entries in these fields.
Detailed in the subsections below are the per-component specific fields that are required.

\begin{table}[!h]
  \label{tab:commonpart}
  \caption{Common fields for the \texttt{<PART>} table}
  \begin{tabular}[!h]{r|l}
    Component & Description\\
    \hline
    \hline
    OptoHybrid & \texttt{<PART>} table \\
    GEB        & \texttt{<PART>} table \\
    VFAT       & \texttt{<PART>} table \\
    GBTx       & \texttt{<PART>} table \\
    FEAST      & not tracked in the DB \\
    VTRx       & not tracked in the DB \\
    VTTx       & not tracked in the DB \\
    \hline
  \end{tabular}
\end{table}

\begin{table}[!h]
  \label{tab:gemelectronics}
  \caption{GEM electronics components}
  \begin{tabular}[!h]{r|l}
    Component & Description\\
    \hline
    \hline
    OptoHybrid & \texttt{<PART>} table \\
    GEB        & \texttt{<PART>} table \\
    VFAT       & \texttt{<PART>} table \\
    GBTx       & \texttt{<PART>} table \\
    FEAST      & not tracked in the DB \\
    VTRx       & not tracked in the DB \\
    VTTx       & not tracked in the DB \\
    \hline
  \end{tabular}
\end{table}

\subsubsection{Component tables}

\subsubsection{\texttt{GEB}}
The GEM electronics board (GEB) bridges the chamber and the front-end electronics.
It provides LV power to the front-end electronics, and provides the optical data path for fast and slow control, as well as readout of trigger and tracking data.
For GE1/1 there are 4 types of board, depending on the type of detector (long or short) and the half of the chamner (wide or narrow).
For GE2/1 there are 8 types, again depending on the type of detector.
Short GE2/1 chambers have types \texttt{M1}, \texttt{M2}, \texttt{M3}, \texttt{M4}, while long GE2/1 chambers have types \texttt{M5}, \texttt{M6}, \texttt{M7}, \texttt{M8}.

On each GEB are mounted the FEAST modules that distribute the power.

\subsubsection{\texttt{OptoHybrid}}
The \texttt{OptoHybrid} is a concentrator card that is physically mounted on top of the GEB.
For GE1/1, this board sits at the intersection of two GEB boards, while for GE2/1 the board is mounted onto a single GEB (there is no \texttt{OptoHybrid} for ME0).
On the \texttt{OptoHybrid} are mounted the \texttt{GBTx}, \texttt{FPGA}, \texttt{SCA}, \texttt{VTRx}, and \texttt{VTTx}.
The FPGA is responsible for monitoring and trigger data, and it receives timing configuration data at runtime.

\subsubsection{\texttt{VFAT}}
The front-end readout chip, the VFAT3, is mounted to bridge the GEM readout board and the GEB.
A hybrid connector connects the two domains.
The VFAT configuration is broken into two parts: the chip conifguration, and the channel configuration.
The VFAT has a number of internal circuits that each have a DAC that is calibrated.

\subsubsection{\texttt{GBTx}}
The \texttt{GBTx} chip is used for GE1/1 and GE2/1 to link the back- and front-end via optical links.
They are physically soldered onto the \texttt{OptoHybrid}
The \texttt{GBTx} receives a minimal configuration that is fused into its registers; additional configuration parameters are loaded at runtime.

\subsubsection{\texttt{SCA}}
No serial number.

\subsubsection{\texttt{VTTx}}
No serial number.

\subsubsection{\texttt{VTRx}}
No serial number.

\begin{table}[!h]
  \label{tab:geb}
  \begin{tabular*}{\textwidth}{l@{\extracolsep{\fill}}|l}
    Entry & Description \\
    \hline
    \hline
    Chamber          & primary key, foreign key link to primary key of the chamber table \\
    Test ID          & primary key, incrementing number for this chamber \\
    e-log link       & text field for an e-log short URL \\
    %% test w/ detector & \texttt{boolean} \\
    test w/ cooling  & \texttt{boolean} \\
    test w/ chimney  & \texttt{boolean} \\
    needs hospital   & \texttt{boolean}, investigation required \\
    %% ready for retest & \texttt{boolean}, chamber is out of the hospital and ready to be tested \\
    QC7 completed    & \texttt{boolean}, QC7 for this detector is done \\
    s-curve ``scandate''                & text field, required format \texttt{YYYY.MM.DD.hh.mm} \\
    \texttt{THR\_ARM\_DAC} ``scandate'' & text field, required format \texttt{YYYY.MM.DD.hh.mm} \\
    s-bit rate ``scandate''             & text field, required format \texttt{YYYY.MM.DD.hh.mm} \\
    \# \texttt{VFAT}s replaced          & computed from \# components marked as replaced \\
    \# \texttt{FEAST}s replaced         & computed from \# components marked as replaced \\
    \# \texttt{VTRx}s replaced          & computed from \# components marked as replaced \\
    \# \texttt{VTTx}s replaced          & computed from \# components marked as replaced \\
    \# \texttt{OptoHybrid}s replaced    & computed from \# components marked as replaced \\
    \# \texttt{GEB}s replaced           & computed from \# components marked as replaced \\
    Comment                             & text field for an arbitrary length character comment\\
    \hline
  \end{tabular*}
\end{table}

\newpage

\section{Requirements}
\label{sec:requirements}

\subsection{Loaders}
\label{subsec:requirements:load}
Two modes of loading data into the database are expected:
\begin{itemize}
\item Manually uploading through a web-based GUI, process described in detail in Section~\ref{subsec:requirements:load:webui}
\item Batch uploading through a CLI using XML templates specific to the table and data being loaded
\end{itemize}

\subsubsection{Web UI}
\label{subsec:requirements:load:webui}
The web based UI should have an interface similar to that shown in Fig.~\ref{fig:qc7ui}.
The workflow of the shifter would be the following
\begin{enumerate}
\item Take newly assembled chamber
\item Perform QC7 procedure
\item Make an elog report
\item Open the UI to make the report  
\end{enumerate}
This sequence may be repeated up to 3 times for a perfect chamber, while for chambers with problems, the procedure may be repeated more often.

\textbf{Making a QC7 report}
\begin{enumerate}
\item Select the chamber under test from the dropdown menu (this menu should only be populated from assembled detectors in the DB)
\item Put the elog short URL in the ``elog'' field
\item If uploading historical data, input the ``Test date'', otherwise this will default to the current date
\item Mark the appropriate checkbox in the ``Test type'' box
  \begin{itemize}
  \item First test with electronics on chamber $\rightarrow$ ``On-detector''
  \item Test done with cooling circuit mounted $\rightarrow$ ``w/cooling''
  \item Test done after the chimney has been mounted $\rightarrow$ ``w/chimney''
  \end{itemize}
\item If scans have been successfully taken, the ``scandate'' should be entered into the ``Scans'' box in the appropriate field. This will be mapped to the individual data fields listed in Table~\ref{tab:qc7results}, and validation should be done to ensure the format is correct
\item If component(s) is(are) deemed to need replacement, the appropriate checkbox is selected and the relevant component replacement area is(are) made editable. This procedure is further described in the ``component replacement'' workflow
\item Enter a descriptive comment for the current test result being uploaded
\item Mark the appropriate checkbox in the ``status'' box
  \begin{itemize}
  \item Problem $\rightarrow$ ``Needs hospital''
  \item QC7 finished $\rightarrow$ ``QC7 finished''
  \end{itemize}
\item Click ``Submit'' to upload the test result
\end{enumerate}

\begin{figure}[!h]
\includegraphics[width=\textwidth,keepaspectratio]{graphics/QC7_interface.pdf}
\label{fig:qc7ui}
\caption{Sketch of the QC7 UI}
\end{figure}

\textbf{Component swaping (done for each component needing replacement)}
\begin{enumerate}
\item Select the component that needs replacement. On submission, this action \textbf{must} ``break'' the association of the component to the currently selected chamber
\item In the dropdown, select the component that is replacing the bad component.  On submission, this action \textbf{must} link this new component in the heirarchy of the chamber
\item In the ``Reason'' field, write the reason that the component is being replaced. This text field \textbf{must} be attached to the \texttt{<PART>} table associated with the specific component being replaced, and \textit{may} appended to the ``Comment'' in the QC7 results table
\end{enumerate}

\subsubsection{\texttt{dbldr}}
\label{subsec:requirements:load:dbldr}
The \texttt{gemdbldr} is a dropbox style file watcher process, running in a look-area to watch for new files.
The files must be zipped, and the XML must be valid, corresponding to the table where the dataset is destined.
It is expected that the DB group provides the template for this upload.

\newpage

\subsection{Extraction}
\label{subsec:requirements:extract}
Extraction is most important for configuration and visualization.

\newpage

\subsection{Validation}
\label{subsec:requirements:validate}
Tools to ensure the validity of the data in the database, as well as the consistency of the data to be loaded are expected.

\newpage

\subsection{Visualization}
\label{subsec:requirements:visuzlize}
The visualization will be done with the CMS online monitoring system (OMS).

\subsubsection{\textbf{Integrated QC7 completion and time-to-completion projection}}

\subsubsection{\textbf{Per-week throughput}}

\subsubsection{\textbf{Integrated failed components}}

\subsubsection{\textbf{table of latest QC7 entry for each chamber}}

\subsubsection{\textbf{table of all QC7 entries for a selected chamber}}

\subsubsection{\textbf{desired visualization}}


\newpage

\section{Status}
\label{sec:status}

\end{document}
