SPECIFICATIONS:= gem-chamber-assembly-db-specs \
	gem-db-spec-doc \
	gem-installation-db-specs \
	gem-electronics-components-specs \
	gem-fw-rpm-spec \
	gem-monitoring-spec \
	gem-qc7-db-specs \

SPECIFICATIONS.PDF     := $(patsubst %,%.pdf,     $(SPECIFICATIONS))
SPECIFICATIONS.CLEAN   := $(patsubst %,%.clean,   $(SPECIFICATIONS))
SPECIFICATIONS.CLEANALL:= $(patsubst %,%.cleanall,$(SPECIFICATIONS))

MAKE:=pdflatex -shell-escape -synctex=1 -file-line-error
# MAKE:=pdflatex -shell-escape -synctex=1 -file-line-error -max-in-open=100 -pdf -bibtex
.PHONY: all pdf clean cleanall
default: all

$(SPECIFICATIONS.PDF):
	$(MAKE) $(patsubst %.pdf,%, $@)

ARTIFACTS:= aux fdb_latexmk fls log out synctex.gz

$(SPECIFICATIONS.CLEAN):
	$(RM) $(foreach artifact,$(ARTIFACTS), $(patsubst %.clean,%, $@).$(artifact))

$(SPECIFICATIONS.CLEANALL): $(SPECIFICATIONS.CLEAN)
	$(RM) $(patsubst %.cleanall,%, $@).pdf

.PHONY: $(SPECIFICATIONS.PDF) $(SPECIFICATIONS.CLEAN)

pdf: $(SPECIFICATIONS.PDF)

all: $(SPECIFICATIONS.PDF)

clean: $(SPECIFICATIONS.CLEAN)

cleanall: $(SPECIFICATIONS.CLEANALL)
