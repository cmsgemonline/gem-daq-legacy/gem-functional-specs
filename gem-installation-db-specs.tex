\documentclass[10pt,a4paper]{refart}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[english]{isodate}
\usepackage[parfill]{parskip}

\usepackage{float}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{color,xcolor}
\usepackage{textcomp,tipa}

\usepackage{amssymb}
\usepackage[normalem]{ulem}
\usepackage{hyperref}
\usepackage{feynmp}
\usepackage{etoolbox}
\usepackage{ifpdf}
\ifpdf
\DeclareGraphicsRule{*}{mps}{*}{}
\fi
\usepackage{multirow}
\usepackage{subfigure}
\usepackage{pdflscape}
%% \usepackage{subfig}
\usepackage{textpos}
\usepackage{pdfpages}
\usepackage{tikz}

\usepackage{pgfplots}
\usetikzlibrary{positioning}
% ,positioning-plus,node-families}
% \usetikzlibrary{geometric,fit}
\usetikzlibrary{fit}
\usetikzlibrary{backgrounds}
\usetikzlibrary{calc}
\usetikzlibrary{shapes,shadows,arrows}
\usetikzlibrary{mindmap}
\usetikzlibrary{decorations.text}
\pgfplotsset{compat=1.7}

\title{GEM Chamber Installation DB Functional Specifications}
\author{GEM Commissioining, Operations, and Online Software Group}
\date{\today}

\begin{document}

\maketitle

\section*{Outline}
\label{sec:outline}
This document aims to provide a concise set of functional database requirements for the super chamber installation and TC commissioning needs of the GEM project.

\input{gem-spec-common-outline}

\input{gem-spec-common-terminology}

\newpage

\section{Details}
\label{sec:details}

\subsection{Tables}
\label{subsec:details:tables}

\subsubsection{Component tables}
The individual (tracked) components are listed in Table~\ref{tab:commonpart}.
\begin{table}[!h]
  \label{tab:commonpart}
  \caption{DB tables relevant for the installation activities}
  \begin{tabular}[!h]{p{0.2\textwidth}|p{0.8\textwidth}}
    Table & Description\\
    \hline
    \hline
    SuperChamber        & \texttt{PARTS} table \\
    Chamber             & \texttt{PARTS} table \\
    Installation Status & \textbf{\textcolor{green}{NEW}} table \\
    \hline
  \end{tabular}
\end{table}


\subsubsection{\texttt{Installation status}}
Short SuperChambers are first inserted into the CMS endcap, and once two short SCs are inserted it is possible to insert the long SC in between.
Subsequently, the LV and HV cables are routed and plugged into the on-detector patch panel.
Likewise for the optical fibres.
All of these connections are also made through UXC and USC to their respective back-end connections.
Gas is connected once all chambers of the circuit are installed, but may not be turned on until later, and a leak test is performed.
Cooling is likewise, and must pass a pressure test.
The initial connectivity test is done simply to check that the elctronics are operational.
the RADMON cable will be connected to certain chambers for radiation monitoring.
There are four temperature chains, \textbf{\textcolor{red}{RED}}, \textbf{\textcolor{green}{GREEN}}, \textbf{\textcolor{blue}{BLUE}}, and \textbf{\textcolor{yellow}{YELLOW}}, depending on the chamber (information stored in the chamber table).
Following this set of steps, the chamber is considered installed, and the commssioning takes over.

The REQUIRED information is listed in Table~\ref{tab:installation}.
Several optimizations could be foreseen, namely, in the Chamber table/view, the ``Temperature Sensor ID'' and ``Temperature Chain ID'' MAY be redundant, the ``Temperature Chain'' SHOULD be defined in the ``Temperature Sensor'' table, as a FK linked via the ID.

\begin{table}[!h]
  \label{tab:installation}
  \caption{Information REQUIRED for the SuperChamber installation table/view}
  \begin{tabular}[!h]{p{0.4\textwidth}|p{0.6\textwidth}}
    %\begin{tabular*}{\textwidth}{l@{\extracolsep{\fill}}|l}
    Entry & Description \\
    \hline
    \hline
    SC ID                        & PK, S/N of chamber, .e.g., \texttt{GE1/1-X-L-CERN-0028} \\
    CMS position                 & Position of the chamber, .e.g., \texttt{GE1/1/M/01} \\
    SC Inserted                  & \texttt{boolean} \\
    GAS connected                & \texttt{boolean} \\
    GAS leak test passed         & \texttt{boolean} \\
    Cooling connected            & \texttt{boolean} \\
    Cooling pressure test passed & \texttt{boolean} \\
    LV connected                 & \texttt{boolean} \\
    HV connected                 & \texttt{boolean} \\
    GEM fibres connected         & \texttt{boolean} \\
    CSC fibres connected         & \texttt{boolean} \\
    Temp chain connected         & \texttt{boolean}, NULLable (only for short chambers) \\
    RADMON connected             & \texttt{boolean}, NULLable (only on \textbf{certain} long chambers\\
    e-log link                   & text field for an e-log short URL \\
    Comment                      & text field for an arbitrary length character comment\\
    \hline
  \end{tabular}
\end{table}



\newpage
\section{Requirements}
\label{sec:requirements}

\subsection{Loaders}
\label{subsec:requirements:load}
Two modes of loading data into the database are expected:
\begin{itemize}
\item Manually uploading through a web-based GUI, process described in detail in Section~\ref{subsec:requirements:load:webui}
\item Batch uploading through a CLI using XML templates specific to the table and data being loaded
\end{itemize}

\subsubsection{Web UI}
\label{subsec:requirements:load:webui}
The primary interactions with the web UI is foreseen to be a crucial part of the normal chamber assembly workflow.
\begin{enumerate}
\item Initial loading of primary data associated with the SC installation itself (chamber inserted)
\item Subsequent updating of data, i.e., when electronics connectivity is done, cables are connected, etc., (cf.~Table~\ref{tab:installation}
\end{enumerate}


\subsubsection{\texttt{dbldr}}
\label{subsec:requirements:load:dbldr}
The \texttt{gemdbldr} is a dropbox style file watcher process, running in a look-area to watch for new files.
The files MUST be zipped, and the XML MUST be valid, corresponding to the table where the dataset is destined.
It is expected that the DB group provides the template for this upload, with the input from the installation team who are currently keeping this information in spreadheet files.

\newpage

\subsection{Extraction}
\label{subsec:requirements:extract}
Extraction is most important for configuration and visualization.

\newpage

\subsection{Validation}
\label{subsec:requirements:validate}
Tools to ensure the validity of the data in the database, as well as the consistency of the data to be loaded are expected.

\newpage

\subsection{Visualization}
\label{subsec:requirements:visuzlize}
The visualization will be done with the CMS online monitoring system (OMS).

\subsubsection{\textbf{Disk view of status}}

\subsubsection{\textbf{Per-week throughput}}

\subsubsection{\textbf{table of latest entry for each SC}}

\subsubsection{\textbf{table of all entries for a selected SC}}

\subsubsection{\textbf{<other desired visualization>}}


\newpage

\section{Use cases}
\label{sec:usecases}

\newpage

\section{State of the art}
\label{sec:status}

\end{document}
