\documentclass[10pt,a4paper]{refart}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[english]{isodate}
\usepackage[parfill]{parskip}

\usepackage{float}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{color}
\usepackage{textcomp,tipa}

\usepackage{amssymb}
\usepackage[normalem]{ulem}
\usepackage{hyperref}
\usepackage{feynmp}
\usepackage{etoolbox}
\usepackage{ifpdf}
\ifpdf
\DeclareGraphicsRule{*}{mps}{*}{}
\fi
\usepackage{multirow}
\usepackage{subfigure}
\usepackage{pdflscape}
%% \usepackage{subfig}
\usepackage{textpos}
\usepackage{pdfpages}
\usepackage{tikz}

\usepackage{pgfplots}
\usetikzlibrary{positioning}
% ,positioning-plus,node-families}
% \usetikzlibrary{geometric,fit}
\usetikzlibrary{fit}
\usetikzlibrary{backgrounds}
\usetikzlibrary{calc}
\usetikzlibrary{shapes,shadows,arrows}
\usetikzlibrary{mindmap}
\usetikzlibrary{decorations.text}
\pgfplotsset{compat=1.7}

\title{GEM QC7 DB Functional Specifications}
\author{GEM Commissioining, Operations, and Online Software Group}
\date{\today}

\begin{document}

\maketitle

\section*{Outline}
\label{sec:outline}
This document aims to provide a concise set of functional requirements for the QC7 needs of the GEM project.
QC7 is done following the assembly of the detector and testing of the chamber properties, the mounting of electronics is the first step, followed by the mounting of the cooling circuit, and terminated after the mounting of the aluminum chimney.
There is an expected amount of overlap with the functional specification outlined in the GEM Chamber Assembly DB Functional Specification, as the Assembly Team is responsible for mounting the detector components, the cooling circuit, and the aluminum chimney.
The tooling SHOULD be designed in such a way as to remove duplication of functinoaity, and aim to provide a single, user-friendly interface to the whole process.

\input{gem-spec-common-outline}

\input{gem-spec-common-terminology}

\newpage

\section{Details}
\label{sec:details}
The QC7 procedure is done on a single detector, instrumented with electronics.
It is expected that when the detector is ready for QC7, the initial set of electronics components have been ``attached'' in the database by the assembly team as per the corresponding functional specification document~\cite{gem-chamber-assembly-spec}.
During the QC7 processes, the cooling plate is attached, and finally the aluminum chimney is attached.
A set of tests is run during each stage, to diagnose any issues with the electronics.
Individual electronics components MAY be swapped or removed.

\subsection{Tables (or Views)}
\label{subsec:details:tables}
\subsubsection{Component tables}
\begin{table}[!h]
  \begin{tabular}[!h]{r|l}
    Component & Description\\
    \hline
    \hline
    OptoHybrid & \texttt{PARTS} table \\
    GEB        & \texttt{PARTS} table \\
    VFAT       & \texttt{PARTS} table \\
    GBTx       & \texttt{PARTS} table \\
    FEAST      & not tracked in the DB \\
    VTRx       & not tracked in the DB \\
    VTTx       & not tracked in the DB \\
    \hline
  \end{tabular}
\end{table}

\subsubsection{Other tables (or views)}
\begin{table}[!h]
  \begin{tabular}[!h]{r|l}
    Table & Description\\
    \hline
    \hline
    Chamber     & Links together all components into a single GEM chamber\\
    QC7 results & Summary table for the information related to a single QC7 test\\
    \hline
  \end{tabular}
\end{table}

\subsubsection{\texttt{Chamber}}
The purpose of this table is to link together all individual components that make up a GEM chamber.
This includes the electronics (GEB, OptoHybrid, VFAT, GBTx, FEAST, VTTx, VTRx), the readout board (ROB), the GEM foils, the cooling circuit, the aluminum frame, the chimney, the drift board, the temperature sensors (and corresponding chain), and the RADMON sensors (if present).
This table is described in detail in the Chamber Assembly specification document~\cite{gem-chamber-assembly-spec}.

\subsubsection{\texttt{QC7 results}}
The QC7 results table is the only new table that is specific to this use case.
The purpose of this table is to link together all information that is recorded during a QC7 of a given detector.
This table has the REQUIRED entities outlined in Table~\ref{tab:qc7results}.
\begin{table}[!h]
  \label{tab:qc7results}
  \begin{tabular*}{\textwidth}{l@{\extracolsep{\fill}}|l}
    Entry & Description \\
    \hline
    \hline
    Chamber ID       & PK, FK link to PK of the chamber table \\
    Test ID          & PK, incrementing number for this chamber \\
    e-log link       & text field for an e-log short URL \\
    %% test w/ detector & \texttt{boolean} \\
    test w/ cooling  & \texttt{boolean} \\
    test w/ chimney  & \texttt{boolean} \\
    needs hospital   & \texttt{boolean}, investigation required \\
    %% ready for retest & \texttt{boolean}, chamber is out of the hospital and ready to be tested \\
    QC7 completed    & \texttt{boolean}, QC7 for this detector is done \\
    s-curve ``scandate''                & text field, REQUIRED format \texttt{YYYY.MM.DD.hh.mm} \\
    \texttt{THR\_ARM\_DAC} ``scandate'' & text field, REQUIRED format \texttt{YYYY.MM.DD.hh.mm} \\
    s-bit rate ``scandate''             & text field, REQUIRED format \texttt{YYYY.MM.DD.hh.mm} \\
    \# \texttt{VFAT}s replaced          & computed from \# VFATs marked as replaced \\
    \# \texttt{FEAST}s replaced         & computed from \# FEASTs marked as replaced \\
    \# \texttt{VTRx}s replaced          & computed from \# VTRxs marked as replaced \\
    \# \texttt{VTTx}s replaced          & computed from \# VTTxs marked as replaced \\
    \# \texttt{OptoHybrid}s replaced    & computed from \# OptoHybrids marked as replaced \\
    \# \texttt{GEB}s replaced           & computed from \# GEBs marked as replaced \\
    Comment                             & text field for an arbitrary length character comment\\
    \hline
  \end{tabular*}
\end{table}

\newpage

\section{Requirements}
\label{sec:requirements}

\subsection{Loaders}
\label{subsec:requirements:load}
Two modes of loading data into the database are expected:
\begin{itemize}
\item Manually uploading through a web-based GUI, process described in detail in Section~\ref{subsec:requirements:load:webui}
\item Batch uploading through a CLI using XML templates specific to the table and data being loaded
\end{itemize}

\subsubsection{Web UI}
\label{subsec:requirements:load:webui}
The web based UI SHOULD have an interface similar to that shown in Fig.~\ref{fig:qc7ui}.
The workflow of the shifter would be the following
\begin{enumerate}
\item Take newly assembled chamber
\item Perform QC7 procedure
\item Make an elog report
\item Open the UI to make the report  
\end{enumerate}
This sequence MAY be repeated up to 3 times for a perfect chamber, while for chambers with problems, the procedure MAY be repeated more often.

\textbf{Making a QC7 report}
\begin{enumerate}
\item Select the chamber under test from the dropdown menu (this menu SHOULD only be populated from assembled detectors in the DB)
\item Put the elog short URL in the ``elog'' field
\item If uploading historical data, input the ``Test date'', otherwise this will default to the current date
\item Mark the appropriate checkbox in the ``Test type'' box
  \begin{itemize}
  \item First test with electronics on chamber $\rightarrow$ ``On-detector'', this box should remain checked for all subsequent tests, unless it is explicitly unselected (value filled from info already in the DB)
  \item Test done with cooling circuit mounted $\rightarrow$ ``w/cooling'', SHOULD activate a dropdown box to select the cooling circuit mounted, by S/N, which SHOULD be added to the ``QC7 Report'' table, this box should remain checked for all subsequent reports, unless it is explicitly unchecked, thereby breaking the connection with the cooling circuit.  The cooling circuit MAY be replaced in the subsequent test if a problem was detected.
  \item Test done after the chimney has been mounted $\rightarrow$ ``w/chimney'', MAY activate a dropdown box to select the chimney mounted, by S/N, which MAY be added to the ``QC7 Report'' table, this box should remain checked for all subsequent tests, unless it is explicitly unselected (value filled from info already in the DB)
  \end{itemize}
\item If scans have been successfully taken, the ``scandate'' SHOULD be entered into the ``Scans'' box in the appropriate field. This will be mapped to the individual data fields listed in Table~\ref{tab:qc7results}, and validation SHOULD be done to ensure the format is correct
\item If component(s) is(are) deemed to need replacement, the appropriate checkbox is selected and the relevant component replacement area is(are) made editable. This procedure is further described in the ``component replacement'' workflow
\item Enter a descriptive comment for the current test result being uploaded
\item Mark the appropriate checkbox in the ``status'' box
  \begin{itemize}
  \item Problem $\rightarrow$ ``Needs hospital''
  \item QC7 finished $\rightarrow$ ``QC7 finished''
  \end{itemize}
\item Click ``Submit'' to upload the test result and populate the DB
\end{enumerate}

\begin{figure}[!h]
  \hspace*{-30ex}\includegraphics[width=1.5\textwidth,keepaspectratio]{graphics/QC7_interface.pdf}
\label{fig:qc7ui}
\caption{Sketch of the QC7 UI}
\end{figure}

\textbf{Component swaping (done for each component needing replacement)}
\begin{enumerate}
\item Select the component that needs replacement. On submission, this action MUST ``break'' the association of the component to the currently selected chamber heirarchy.
\item In the dropdown, select the component that is replacing the bad component.  On submission, this action MUST link this new component in the heirarchy of the chamber.
\item In the ``Reason'' field, write the reason that the component is being replaced. This text field MUST be attached to the \texttt{PARTS} table associated for the specific component being replaced, and MAY appended to the ``Comment'' in the QC7 results table (especially in the case that the part being replaced is not tracked in the DB, e.g., FEAST, VTRx, VTTx)
\end{enumerate}

\textbf{Example: replacing VFATs}
\begin{enumerate}
\item Mark the VFAT checkbox
\item The list will have the columns of ``X'', ``position'', ``old VFAT S/N'' (as readonly text field), ``new VFAT S/N'' (as a dropdown), and ``Reason'' (as editable text field)
\item Click the checkbox next to the corresponding position(s)
\item For each selected position, select the new VFAT (``new S/N'') that is replacing the previous component (``old S/N''), and write the reason in the box
\item Submit
\end{enumerate}

\textbf{Example: replacing GEB}
\begin{enumerate}
\item Mark the GEB checkbox
\item Select which GEB(s) is(are) to be replaced (N/W for GE1/1, MX for GE2/1, but this information SHOULD be dynamically displayed based on the chamber name)
  \begin{enumerate}
  \item N.B. replacement of a GEB implies detatchment of VFATs and reattachment. To facilitate the replacement in the same positions, the VFAT box should now be populated with the currently attached VFATs
  \end{enumerate}
\item Submit
\end{enumerate}

\subsubsection{\texttt{dbldr}}
\label{subsec:requirements:load:dbldr}
The \texttt{gemdbldr} is a dropbox style file watcher process, running in a look-area to watch for new files.
The files MUST be zipped, and the XML MUST be valid, corresponding to the table where the dataset is destined.
It is expected that the DB group provides the template for this upload, corresponding to a complete QC7 report.

\newpage

\subsection{Extraction}
\label{subsec:requirements:extract}
Extraction is most important for configuration and visualization.

\newpage

\subsection{Validation}
\label{subsec:requirements:validate}
Tools to ensure the validity of the data in the database, as well as the consistency of the data to be loaded are expected.

\newpage

\subsection{Visualization}
\label{subsec:requirements:visuzlize}
The visualization will be done with the CMS online monitoring system (OMS).

\subsubsection{\textbf{Integrated QC7 completion and time-to-completion projection}}

\subsubsection{\textbf{Per-week throughput}}

\subsubsection{\textbf{Integrated failed components}}

\subsubsection{\textbf{table of latest QC7 entry for each chamber}}

\subsubsection{\textbf{table of all QC7 entries for a selected chamber}}

\subsubsection{\textbf{Plot of number of QC7 steps per chamber}}

\subsubsection{\textbf{desired visualization}}

\newpage

\section{Use cases}
\label{sec:usecases}

\newpage

\section{State of the art}
\label{sec:status}

\end{document}
